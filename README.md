INSTALL ECLIPSE
- download eclipse 2019.03 from https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-03/R/eclipse-inst-win64.exehttps://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-03/R/eclipse-inst-win64.exe
- Run the installer and select Eclipse IDE for Enterprise Java Developers
- Open Eclipse after installing it
- Open a workspace in eclipse

IMPORT THIS PROJECT
- Select File->Import from the menu
- Select Gradle and Existing Gradle Projects
- Navigate to the demo subfolder within the folder where this project was extracted
- Make sure pom.xml is selected
- Select Finish

RUN THE WEBAPP
- run 'gradlew bootRun' from the command line to run the app
- Go to http://localhost:8080/h2-console/ 
- change the JDBC URL to jdbc:h2:mem:testdb if it is not already set
- connect with Postman or app of your choice to make POST, PUT, and DELETE commands
- 

RUN TESTS
- run 'gradlew test' from the command line in the base directory to run the tests


COMMANDS
- to insert a movie into the database
  POST url: /api/movie to insert a movie into the database
        sample body:
        {
            "name" : "test movie",
            "genre" : "rom/com",
            "year" : 2010,
            "rating" : "PG-13"
        }

- to list move details for the first movie in the database
  GET url: /api/movie/1 

- to modify the first movie in the database
  PUT url: /api/movie/1
        sample body:
        {
            "name" : "test movi correction",
            "genre" : "rom/com",
            "year" : 2010,
            "rating" : "PG-13"
        }

- to delete the first movie from the database
  DELETE url: /api/movie/1

- to get the complete list of movies in the databse
  GET url: /api/movie/list

- to get the current time
  GET url: /api/timeOfDay

NOTES
- started with Initializr at https://start.spring.io/
- ideally we would use slugs rather than primary keys, and things like rating would be normalized into a lookup table (if using a relational db)
- Unit test are a bit sparse, but there are a couple of positive and negative test cases as well as integration testing of the controller in addition to standard Unit tests