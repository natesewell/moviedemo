package com.example.movies;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import com.example.movies.controllers.MovieController;
import com.example.movies.models.Movie;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovieControllerTests {
	@Autowired
	MovieController movieController;
	
	@Test
	public void emptyListTest() {
		Assert.assertTrue("Movie List is not empty", movieController.getList().isEmpty());
	}
	
	@Test
	public void insertTest() {
		Movie movie = new Movie();
		movie.setName("test name");
		movie.setGenre("rom/com");
		movie.setRating("PG-13");
		movie.setYear(2000);
		
		// check for problems
		ResponseEntity<String> response = movieController.createMovie(movie);
		Assert.assertTrue("Error inserting movie " + response.getStatusCode(), 
				response.getStatusCode() == HttpStatus.OK);
		
		// cleanup
		List<Movie> movies = movieController.getList();
		movieController.deleteMovie(movies.get(0).getId().toString());
	}
	
	@Test 
	public void deleteTest() {
		Movie movie = new Movie();
		movie.setName("test name");
		movie.setGenre("rom/com");
		movie.setRating("PG-13");
		movie.setYear(2000);
		
		movieController.createMovie(movie);
		
		// check that deletion worked
		List<Movie> movies = movieController.getList();
		ResponseEntity<String> response = movieController.deleteMovie(movies.get(0).getId().toString());
		Assert.assertTrue("Error deleting movie " + response.getStatusCode(), 
				response.getStatusCode() == HttpStatus.OK);
		
	}
}
