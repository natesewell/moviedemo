package com.example.movies;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.movies.models.Movie;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovieTests {

	@Test
	public void testInvalidName() {
		Movie movie = new Movie();

		// don't set any name, check for null
		Assert.assertTrue("Invalid name not detected", 
				movie.validate().contains(Movie.INVALID_NAME));
		
		// set a blank name
		movie.setName("");
		Assert.assertTrue("Invalid name not detected", 
				movie.validate().contains(Movie.INVALID_NAME));
	}
	
	@Test
	public void testValidName() {
		Movie movie = new Movie();

		// set a blank name
		movie.setName("test name");
		Assert.assertFalse("Valid name not detected", 
				movie.validate().contains(Movie.INVALID_NAME));
		
	}

}
