package com.example.movies.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.util.StringUtils;

@Entity
@Table(name = "MOVIE")
public class Movie {
	public static final String INVALID_NAME = "Invalid Name";
	public static final String INVALID_GENRE = "Invalid Genre";
	public static final String INVALID_RATING = "Invalid Rating";
	public static final String INVALID_YEAR = "Invalid Release YearY";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	private String genre;
	private Integer year;
	private String rating;

	// getters and setters
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
    
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public String getGenre() {
		return genre;
	}
    
	public void setYear(Integer year) {
		this.year = year;
	}
	
	public Integer getYear() {
		return year;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
	
	public String getRating() {
		return rating;
	}

	/**
	 *  converts the movie data to string format
	 *  @return  
	 */
    @Override
    public String toString() {
    	StringBuilder response = new StringBuilder("{ ");
    	
    	response.append( "\"id\" : \"");
    	response.append(id);
    	response.append("\",");
    	
    	response.append( "\"name\" : \"");
    	response.append(name);
    	response.append("\",");

    	response.append( "\"genre\" : \"");
    	response.append(genre);
    	response.append("\",");
    	
    	response.append( "\"year\" : \"");
    	response.append(year);
    	response.append("\",");

    	response.append( "\"rating\" : \"");
    	response.append(rating);
    	response.append("\"");

    	response.append(" }");
    	
    	return response.toString();
    }
    
    /**
     * checks for errors in the values assigned to the movie
     * 
     * @return String with the a JSON formated list of errors or null if no errors
     */
    public String validate() {    	
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(name)) {
			errors.add(INVALID_NAME);
		}
		if (StringUtils.isEmpty(genre)) {
			errors.add(INVALID_GENRE);
		}
		if (StringUtils.isEmpty(rating)) {
			errors.add(INVALID_RATING);
		}
		if (year == null) {
			errors.add(INVALID_YEAR);
		}
		// check for if any errors occurred return the list if found 
		if (!errors.isEmpty()) {
			boolean firstError = true;
			StringBuilder response = new StringBuilder(" { \"errors\" : [");
			// append each error to the error response 
			for (String error : errors) {
				// do not add a comma to the first item in the list
				if (!firstError) {
					response.append(", ");
				}
				else {
					firstError = false;
				}
				response.append("\"");
				response.append(error);
				response.append("\"");
			}
			response.append("] }");
			return response.toString();
		}
		return null;
    }
}
