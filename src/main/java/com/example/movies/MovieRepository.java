package com.example.movies;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.movies.models.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{
	
}

