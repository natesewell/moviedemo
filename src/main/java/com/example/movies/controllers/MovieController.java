package com.example.movies.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movies.MovieRepository;
import com.example.movies.models.Movie;


@RestController
@RequestMapping(value="/api/movie")

public class MovieController {

	@Autowired
	MovieRepository repository;	
	
	@GetMapping(value="/{id}", produces = "application/json")
	public ResponseEntity<String> getMovie(@PathVariable("id") String id) {
		Long key;
		try {
			key = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			return ResponseEntity.badRequest().body("Invalid Movie Identifier");
		}
		Movie movie;
		try {	
			movie = repository.findById(key).get();
		}
		catch (NoSuchElementException ex) {
			return ResponseEntity.badRequest().body("{\"error\" : \"No Movie Found\" }");
		}
		return ResponseEntity.ok(movie.toString());
	}

	@PostMapping(consumes = "application/json", 
			produces = "application/json")
	public ResponseEntity<String> createMovie(@RequestBody Movie movie) {
		// check that the movie info sent in is valid
		String validationErrors = movie.validate(); 
		if (validationErrors != null) {
			return ResponseEntity.badRequest().body(validationErrors);
		}
		// go ahead and save the movie and return success if there were no errors
		repository.save(movie);
		return ResponseEntity.ok("Created Succcesfully");
	}
	
	@PutMapping(value="/{id}", 
			consumes = "application/json",
			produces = "application/json")
	public ResponseEntity<String> updateMovie(@PathVariable("id") String id,
			@RequestBody Movie movie) {
		Long key;
		try {
			key = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			return ResponseEntity.badRequest().body("{\"error\" : \"Invalid Movie Identifier\" }");
		}
		// check that the movie info sent in is valid
		String validationErrors = movie.validate(); 
		if (validationErrors != null) {
			return ResponseEntity.badRequest().body(validationErrors);
		}

		Movie original;
		try {
			original = repository.findById(key).get();
		}
		catch (NoSuchElementException ex) {
			return ResponseEntity.badRequest().body("{\"error\" : \"No Movie Found\" }");
		}
		original.setName(movie.getName());
		repository.save(original);
		return ResponseEntity.ok("Updated Succcesfully");
	}
	
	@DeleteMapping(value="/{id}", produces = "application/json")
	public ResponseEntity<String> deleteMovie(@PathVariable("id") String id) {
		Long key;
		try {
			key = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			return ResponseEntity.badRequest().body("{\"error\" : \"Invalid Movie Identifier\" }");
		}
		try {
			repository.deleteById(key);
		}
		// if the movie to delete was not found return an error
		catch (EmptyResultDataAccessException ex) {
			return ResponseEntity.badRequest().body("{\"error\" : \"No Movie Found\" }");
		}
		return ResponseEntity.ok("Deleted Succcesfully");
	}
	
	@GetMapping(value="/list", produces = "application/json")
	public List<Movie> getList() {
		List<Movie> movies = repository.findAll();
		return movies;
	}
	
}
