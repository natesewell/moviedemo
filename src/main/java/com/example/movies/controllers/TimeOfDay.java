package com.example.movies.controllers;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.TimeZone;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api")
public class TimeOfDay {
	@GetMapping(value="/timeOfDay", produces = "application/json")
	public ResponseEntity<String> getTime() {
		Calendar now = Calendar.getInstance();
		TimeZone timeZone = now.getTimeZone();
		
		LocalDateTime localDateTime = LocalDateTime.ofInstant(now.toInstant(), 
				timeZone.toZoneId());
		return ResponseEntity.ok("{ \"time\" : \"" + localDateTime.toString() + "\" }");
	}
}
